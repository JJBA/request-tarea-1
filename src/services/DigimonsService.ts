import { DigimonI } from "../interfaces/DigimonInterfaces";
const db = require('../db/Digimons.json');

module DigimonsService { 
    export function getAll(): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        return digimons
    }
    export function get(id: number): DigimonI {
        const digimons: Array<DigimonI> = db;
        const digimon: Array<DigimonI> = digimons.filter(e => e.id === id);
        if (digimon.length < 1) {
            throw "No se encontró el digimon"
        }
        return digimon[0];
    }
    export function getByName(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = digimons.filter(function(el) {
            return el.name.toLowerCase().indexOf(name.toLowerCase()) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        let matches: Array<DigimonI> = [];
        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(digimon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function getstrongAgainst(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = getByName(name);
        let weak: Array<DigimonI> = [];

        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === matches[0].type[0].strongAgainst[0]);
            if (found.length>0) {
                weak.push(digimon);
            }
        })

        if (weak.length < 1) {
            throw matches[0].type[0].strongAgainst[0];
        }

        return weak;
    }

    export function getweakAgainst(name: string): Array<DigimonI> {
        const digimons: Array<DigimonI> = db;
        const matches: Array<DigimonI> = getByName(name);
        let strong: Array<DigimonI> = [];

        digimons.forEach(digimon => {
            const found = digimon.type.filter(e => e.name === matches[0].type[0].weakAgainst[0]);
            if (found.length>0) {
                strong.push(digimon);
            }
        })

        if (strong.length < 1) {
            throw matches[0].type[0].weakAgainst[0];
        }

        return strong;
    }

    export function create(Digimon: DigimonI){
        db.push(Digimon);
        return Digimon;
    }
}

export default DigimonsService;
