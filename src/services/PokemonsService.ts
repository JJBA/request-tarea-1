import { PokemonI } from "../interfaces/PokemonInterfaces";
const db = require('../db/Pokemons.json');

module PokemonsService { 
    export function getAll(): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        return pokemons
    }
    export function get(id: number): PokemonI {
        const pokemons: Array<PokemonI> = db;
        const pokemon: Array<PokemonI> = pokemons.filter(e => e.id === id);
        if (pokemon.length < 1) {
            throw "No se encontró el digimon"
        }
        return pokemon[0];
    }
    export function getByName(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = pokemons.filter(function(el) {
            return el.name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "").indexOf(name.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "")) > -1;
        })
        if (matches.length < 1) {
            throw "No se encontró el digimon"
        }
        return matches;
    }
    
    export function getByType(type: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        let matches: Array<PokemonI> = [];
        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === type);
            if (found.length>0) {
                matches.push(pokemon);
            }
        })
         
        if (matches.length < 1) {
            throw "No se encontró el tipo"
        }
        return matches;
    }

    export function getstrongAgainst(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = getByName(name);
        let weak: Array<PokemonI> = [];

        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === matches[0].type[0].strongAgainst[0]);
            if (found.length>0) {
                weak.push(pokemon);
            }
        })

        if (weak.length < 1) {
            throw matches[0].type[0].strongAgainst[0];
        }

        return weak;
    }

    export function getweakAgainst(name: string): Array<PokemonI> {
        const pokemons: Array<PokemonI> = db;
        const matches: Array<PokemonI> = getByName(name);
        let strong: Array<PokemonI> = [];

        pokemons.forEach(pokemon => {
            const found = pokemon.type.filter(e => e.name === matches[0].type[0].weakAgainst[0]);
            if (found.length>0) {
                strong.push(pokemon);
            }
        })

        if (strong.length < 1) {
            throw matches[0].type[0].weakAgainst[0];
        }

        return strong;
    }

    export function create(Pokemon: PokemonI){
        db.push(Pokemon);
        return Pokemon;
    }
}

export default PokemonsService;