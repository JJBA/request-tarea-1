import { Request, Response } from "express";
import PokemonsService from "../services/PokemonsService";

export function getAll(_: any, res: Response) {
    const pokemons = PokemonsService.getAll();
    res.status(200).json(pokemons);
}

export function get(req: Request, res: Response) {
    try {
        const id = req.params.id && +req.params.id || undefined;
        if(!id){ throw "Se requiere el ID del pokemon."}
        const pokemons = PokemonsService.get(id);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByName(req: Request, res: Response) {
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.getByName(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getByType(req: Request, res: Response) {
    try {
        const type = req.params.type && req.params.type || undefined;
        if(!type){ throw "Se requiere el Tipo del pokemon."}
        const pokemons = PokemonsService.getByType(type);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getstrongAgainst(req: Request, res: Response){
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.getstrongAgainst(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function getweakAgainst(req: Request, res: Response){
    try {
        const name = req.params.name && req.params.name || undefined;
        if(!name){ throw "Se requiere el name del pokemon."}
        const pokemons = PokemonsService.getweakAgainst(name);
        res.status(200).json(pokemons);
    } catch (error) {
        res.status(400).send(error);
    }
}

export function create(req: Request, res:Response){
    try{
        const pokemon = req.body && req.body || undefined;
        if(!pokemon){ throw "No se ha recibido ningun pokemon"}
        res.status(200).json({msg: "Pokemon creado", id:pokemon.id, name: pokemon.name, type:pokemon.type, img: pokemon.img})
    }catch{

    }
}