* Servicio para consultar por nombre (puedo no ponerlo completo y puedo no usar tilde):
/digimons/name/(nombre del digimon) | 
/pokemons/name/(nombre del pokemon)

* Servicio para consultar por tipo:
/digimons/type/(tipo del digimon) | 
/pokemons/type/(tipo del pokemon)

* Servicio para consultar si un digimón/pokemón es fuerte o débil contra otro:
Fuerte contra:
/digimons/strong/(nombre del digimon)
/pokemons/strong/(nombre del pokemon)
Debil contra:
/digimons/weak/(nombre del digimon)
/pokemons/weak/(nombre del pokemon)

* Servicio para crear un nuevo digimon/pokemón (que estará creado en memoria, no debe persistir aún):
Esta creado con un metodo post donde recibe una petición con un id, nombre, tipo, y la imagen del pokemon/digimon.
